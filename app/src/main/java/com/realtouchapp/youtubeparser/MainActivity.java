package com.realtouchapp.youtubeparser;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        View btn_videoview = findViewById(R.id.btn_videoview);
        btn_videoview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, VideoViewActivity.class));
            }
        });

        View btn_exoplayer = findViewById(R.id.btn_exoplayer);
        btn_exoplayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "現在沒功能 (sample code看不太懂...)", Toast.LENGTH_SHORT).show();
            }
        });

    }


}
