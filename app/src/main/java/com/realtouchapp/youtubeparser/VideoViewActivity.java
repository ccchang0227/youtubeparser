package com.realtouchapp.youtubeparser;

import android.app.Activity;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import java.util.HashMap;

import realtouch.youtubeparserkit.core.YoutubeParser;
import realtouch.youtubeparserkit.object.YoutubeVideo;
import realtouch.youtubeparserkit.object.YoutubeVideoQueue;

public class VideoViewActivity extends Activity implements YoutubeVideoQueue.QueueCallback {

    private MyVideoQueue myVideoQueue = new MyVideoQueue();

    private VideoView vv_youtube;
    private TextView v_loading;

    private TextView tv_video_info;

    private View btn_pre;
    private View btn_play;
    private View btn_next;

    private int currentIndex = 0;

    private boolean isPlaying = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_view);

        YoutubeParser.getInstance().setLanguageIdentifier("zh-TW");

        vv_youtube = (VideoView) findViewById(R.id.vv_youtube);
        vv_youtube.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                v_loading.setVisibility(View.GONE);
                isPlaying = true;
            }
        });
        vv_youtube.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                btn_next.performClick();
            }
        });
//        vv_youtube.setOnErrorListener(new MediaPlayer.OnErrorListener() {
//            @Override
//            public boolean onError(MediaPlayer mediaPlayer, int whatError, int extra) {
//                switch (whatError) {
//                    case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
//                        Log.v("Play Error:::", "MEDIA_ERROR_SERVER_DIED");
//                        break;
//                    case MediaPlayer.MEDIA_ERROR_UNKNOWN:
//                        Log.v("Play Error:::", "MEDIA_ERROR_UNKNOWN");
//                        break;
//                    default:
//                        break;
//                }
//                return false;
//            }
//        });
//        vv_youtube.setOnInfoListener(new MediaPlayer.OnInfoListener() {
//            @Override
//            public boolean onInfo(MediaPlayer mediaPlayer, int whatInfo, int extra) {
//                switch(whatInfo){
//                    case MediaPlayer.MEDIA_INFO_BAD_INTERLEAVING:
//                        break;
//                    case MediaPlayer.MEDIA_INFO_METADATA_UPDATE:
//
//                        break;
//                    case MediaPlayer.MEDIA_INFO_VIDEO_TRACK_LAGGING:
//
//                        break;
//                    case MediaPlayer.MEDIA_INFO_NOT_SEEKABLE:
//
//                        break;
//                }
//                return false;
//            }
//        });

        v_loading = (TextView) findViewById(R.id.v_loading);

        tv_video_info = (TextView) findViewById(R.id.tv_video_info);

        btn_pre = findViewById(R.id.btn_pre);
        btn_pre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vv_youtube.stopPlayback();
                v_loading.setVisibility(View.VISIBLE);
                tv_video_info.setText(null);

                isPlaying = false;

                currentIndex --;
                if (currentIndex < 0) {
                    currentIndex = myVideoQueue.size()-1;
                }
                myVideoQueue.loadVideo(currentIndex, VideoViewActivity.this);
            }
        });

        btn_play = findViewById(R.id.btn_play);
        btn_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn_next = findViewById(R.id.btn_next);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vv_youtube.stopPlayback();
                v_loading.setVisibility(View.VISIBLE);
                tv_video_info.setText(null);

                isPlaying = false;

                currentIndex ++;
                if (currentIndex >= myVideoQueue.size()) {
                    currentIndex = 0;
                }
                myVideoQueue.loadVideo(currentIndex, VideoViewActivity.this);
            }
        });

        v_loading.setVisibility(View.VISIBLE);
        tv_video_info.setText(null);

        myVideoQueue.loadVideo(currentIndex, this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        vv_youtube.resume();
    }

    @Override
    protected void onPause() {
        vv_youtube.pause();
        isPlaying = false;

        super.onPause();
    }

    @Override
    protected void onDestroy() {
        vv_youtube.suspend();
        isPlaying = false;

        super.onDestroy();
    }

    private final static String UserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.79 Safari/537.4";

    @Override
    public void onSuccessful(int index, String source, YoutubeVideo video) {
        if (index != currentIndex) {
            return;
        }

        tv_video_info.setText("[" + (index+1) + "]: " + video.getTitle());

        Uri vidUri = video.getPreferredStream().getStreamURL();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            HashMap<String, String> headers = new HashMap<>();
            headers.put("User-Agent", UserAgent);
            vv_youtube.setVideoURI(vidUri, headers);
        }
        else {
            vv_youtube.setVideoURI(vidUri);
        }
        vv_youtube.start();
    }

    @Override
    public void onFailed(int index, String source, String failedReason) {
        Log.e("VideoViewActivity", "failed " + source + " : "+failedReason);
        Toast.makeText(this, failedReason, Toast.LENGTH_SHORT).show();
    }

}
