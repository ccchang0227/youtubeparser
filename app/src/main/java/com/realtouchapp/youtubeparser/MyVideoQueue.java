package com.realtouchapp.youtubeparser;

import java.util.Arrays;

import realtouch.youtubeparserkit.object.YoutubeVideoQueue;

/**
 * Created by C.C.Chang on 2017/2/21.
 *
 * @version 1
 */
@SuppressWarnings("unused")
public class MyVideoQueue extends YoutubeVideoQueue {
    private static String[] urlStrings = {"http://www.youtube.com/c/newsebc/live",
                                            "https://www.youtube.com/watch?v=uCltRSu57RM", // 這部影片含有shemarooent的版權內容，版權擁有者已經因為版權因素封鎖此影片。
                                            "https://youtu.be/b3QIfgD--_E", // 這部影片無法使用
                                            "https://www.youtube.com/watch?v=AuqvEYiMSeE",
                                            "https://www.youtube.com/watch?v=VQsMVE6VZ78", //這個有版權
                                            "https://www.youtube.com/watch?v=Q-C4qqsgs8w",
                                            "https://www.youtube.com/watch?v=kkBdOiaDmRc"};

    private static String[] urlStrings2 = {"6v2L2UGZJAM", // Standard
                                            "rId6PKlDXeU", // VEVO (Mumford & Sons)
                                            "tg00YEETFzg", // VEVO (Rihanna)
                                            "zKovmts2KSk", // Age Restricted
                                            "07FYdnEawAQ", // VEVO + Age Restricted
                                            "JHaA9bKi-xs", // Mobile Restricted
                                            "NUDvGbEFfKk", // Live Video
                                            "1kIsylLeHHU", // Restricted Video
                                            "BXnA9FjvLSU", // Removed Video
                                            "vwkFTztnl7Y" // Geo-Blocked Video
                                            };

    /* 這部影片含有SME擁有的內容，在部分網站上或應用程式中不得播放。
<a href='http://www.youtube.com/watch?v=VQsMVE6VZ78&feature=player_embedded' target='_blank'>在 YouTube 觀賞</a>
 */

    public MyVideoQueue() {
        super(Arrays.asList(urlStrings));
    }

}
