# YoutubeParser

解析youtube原始影片網址的library和sample project。<br>
因為是原始影片檔所以一定不會有廣告。

## Requirements

AndroidOS 4.3 (API 18) 以上

## About Sample Project

#### 內建使用了以下的youtube影片做測試:
- EBC 東森新聞 51 頻道 24 小時線上直播 [http://www.youtube.com/c/newsebc/live](http://www.youtube.com/c/newsebc/live)
- 印度鄭多燕之二部30分鐘釋放運動01 [https://www.youtube.com/watch?v=uCltRSu57RM](https://www.youtube.com/watch?v=uCltRSu57RM)
- 中視新聞台 LIVE直播 [https://youtu.be/b3QIfgD--_E](https://youtu.be/b3QIfgD--_E)
- 鞋貓劍客番外篇:三小惡貓 (內嵌繁中字幕) [https://www.youtube.com/watch?v=AuqvEYiMSeE](https://www.youtube.com/watch?v=AuqvEYiMSeE)
- 鳥的故事 For birds (奧斯卡最佳動畫短片，3'25") [https://www.youtube.com/watch?v=VQsMVE6VZ78](https://www.youtube.com/watch?v=VQsMVE6VZ78)
- Power Rangers (2017 Movie) Official Teaser Trailer – ‘Discover The Power’ [https://www.youtube.com/watch?v=Q-C4qqsgs8w](https://www.youtube.com/watch?v=Q-C4qqsgs8w)
- 10.21《你的名字》中文官方【預告第2彈】⚠️現在已經上映！ [https://www.youtube.com/watch?v=kkBdOiaDmRc](https://www.youtube.com/watch?v=kkBdOiaDmRc)

## Author

Chih-chieh Chang, ccch.realtouch@gmail.com

## License

YoutubeParser is available under the MIT license. See the LICENSE file for more info.
