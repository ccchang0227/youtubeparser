package realtouch.youtubeparserkit.object;

import android.net.Uri;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import realtouch.youtubeparserkit.core.YouTubePlayerScript;
import realtouch.youtubeparserkit.error.NoStreamAvailableException;
import realtouch.youtubeparserkit.error.UseCipherSignatureException;
import realtouch.youtubeparserkit.error.YoutubeErrorException;
import realtouch.youtubeparserkit.error.YoutubeErrorString;

/**
 * Created by Chih-chieh Chang on 2017-2-20.
 *
 * @version 1.0.0
 */
@SuppressWarnings("unused")
public final class YoutubeVideo {

    public final static class Stream {

        private String mTag = null;
        private Uri mStreamURL = null;
        private Map mStreamInfo = null;
        private boolean mIsStream = false;

        Stream(Uri streamURL, String iTag, Map streamInfo, boolean isStream) {
            super();

            mTag = iTag;
            mStreamURL = streamURL;
            mStreamInfo = streamInfo;
            mIsStream = isStream;

        }

        public String getTag() {
            return mTag;
        }

        public Uri getStreamURL() {
            return mStreamURL;
        }

        public String getType() {
            try {
                return mStreamInfo.get("type").toString();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return "unknown type";
        }

        public String getQuality() {
            try {
                String quality = mStreamInfo.get("quality").toString();
                if (null == quality) {
                    quality = mStreamInfo.get("quality_label").toString();
                }
                return quality;
            } catch (Exception e) {
                e.printStackTrace();
            }

            return "unknown quality";
        }

        public boolean isStream() {
            return mIsStream;
        }

    }

    public enum VideoQuality {
        Small240(36),
        Medium360(18),
        HD720(22),
        HD1080(37);

        private int value;
        VideoQuality(int i) {
            this.value = i;
        }
    }

    private String mIdentifier = null;
    private String mTitle = null;
    private double mDuration = 0.0;
    private Uri mSmallThumbnailURL = null;
    private Uri mMediumThumbnailURL = null;
    private Uri mLargeThumbnailURL = null;

    private Stream mPerferredStream = null;
    private Map<String, Stream> mStreams = null;

    private Date mExpirationDate = null;

    private boolean mIsStream = false;
    private Map mVideoParts = null;
    private YouTubePlayerScript mPlayerScript = null;

    public static YoutubeVideo youtubeVideoWith(String videoID, YouTubePlayerScript playerScript, Map videoInfo) throws Exception {
        return parseVideoInfo(videoID, videoInfo, playerScript);
    }

    private YoutubeVideo() {
        super();
    }

    public String getIdentifier() {
        return mIdentifier;
    }

    public String getTitle() {
        return mTitle;
    }

    public double getDuration() {
        return mDuration;
    }

    public Uri getSmallThumbnailURL() {
        return mSmallThumbnailURL;
    }

    public Uri getMediumThumbnailURL() {
        return mMediumThumbnailURL;
    }

    public Uri getLargeThumbnailURL() {
        return mLargeThumbnailURL;
    }

    public Map<String, Stream> getStreams() {
        return mStreams;
    }

    public Date getExpirationDate() {
        return mExpirationDate;
    }

    public boolean isStream() {
        return mIsStream;
    }

    public Stream getPreferredStream() {
        return mPerferredStream;
    }

    // 解析API取得的影片資訊
    private static YoutubeVideo parseVideoInfo(String videoID,
                                               Map info,
                                               YouTubePlayerScript playerScript) throws Exception {
        if (null == info || info.size() == 0) {
            throw new NoStreamAvailableException(YoutubeErrorString.PARSE_FAILED_ERROR);
        }

        String title = (String) info.get("title");
        Date expirationDate = null;
        boolean isStream = false;

        String streamMap = (String) info.get("url_encoded_fmt_stream_map");
        String httpLiveStream = (String) info.get("hlsvp");
        String adaptiveFormats = (String) info.get("adaptive_fmts");
        if ((null != streamMap && streamMap.length() > 0) ||
                (null != httpLiveStream && httpLiveStream.length() > 0)) {
            ArrayList<String> streamQueries = new ArrayList<>();
            if (null != streamMap) {
                streamQueries.addAll(Arrays.asList(streamMap.split(",")));
            }
            if (null != adaptiveFormats) {
                streamQueries.addAll(Arrays.asList(adaptiveFormats.split(",")));
            }

            double duration = 0;
            try {
                duration = Double.parseDouble((String) info.get("length_seconds"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            String smallThumbnail = (String) info.get("thumbnail_url");
            if (null == smallThumbnail) {
                smallThumbnail = (String) info.get("iurl");
            }

            String mediumThumbnail = (String) info.get("iurlsd");
            if (null == mediumThumbnail) {
                mediumThumbnail = (String) info.get("iurlhq");
                if (null == mediumThumbnail) {
                    mediumThumbnail = (String) info.get("iurlmq");
                }
            }
            String largeThumbnail = (String) info.get("iurlmaxres");

            Stream preferredStream = null;
            HashMap<String, Stream> streams = new HashMap<>();
            if (null != httpLiveStream) {
                Uri URI = Uri.parse(httpLiveStream);
                String itag = "HTTPLiveStreaming";
                HashMap<String, String> streamInfo = new HashMap<>();
                streamInfo.put("type", "HTTP Live Streaming");
                Stream stream = new Stream(URI, itag, streamInfo, true);
                streams.put(itag, stream);
                preferredStream = stream;
                isStream = true;
            }

            for (String streamQuery : streamQueries) {
                HashMap<String, String> streamInfo = hashMapFromQueryStringComponents(streamQuery);

                String scrambledSignature = streamInfo.get("s");
                if (null == scrambledSignature) {
                    scrambledSignature = streamInfo.get("sig");
                }
                if (null != scrambledSignature && null == playerScript) {
                    throw new UseCipherSignatureException();
                }

                String signature = null;
                if (null != playerScript) {
                    signature = playerScript.unscrambleSignature(scrambledSignature);
                    if (null != scrambledSignature && null == signature) {
                        continue;
                    }
                }

                String urlString = streamInfo.get("url");
                String itag = streamInfo.get("itag");

                if (null != urlString && null != itag) {
                    Uri streamURI = Uri.parse(urlString);
                    expirationDate = getExpirationDate(streamURI);

                    if (null != signature) {
                        String escapedSignature = stringByEncodingURLFormat(signature);
                        streamURI = URLBySettingParameter(streamURI, "signature", escapedSignature);
                    }

                    streamURI = URLBySettingParameter(streamURI, "ratebypass", "yes");

                    Stream stream = new Stream(streamURI, itag, streamInfo, false);
                    streams.put(itag, stream);

                    if (null == preferredStream) {
                        preferredStream = stream;
                    }
                }
            }

            if (streams.size() == 0) {
                throw new NoStreamAvailableException(YoutubeErrorString.NO_STREAM_AVAILABLE_ERROR);
            }

            YoutubeVideo video = new YoutubeVideo();
            video.mIdentifier = videoID;
            if (null != title) {
                video.mTitle = title;
            }
            else {
                video.mTitle = "";
            }
            video.mDuration = duration;
            if (null != smallThumbnail) {
                video.mSmallThumbnailURL = Uri.parse(smallThumbnail);
            }
            if (null != mediumThumbnail) {
                video.mMediumThumbnailURL = Uri.parse(mediumThumbnail);
            }
            if (null != largeThumbnail) {
                video.mLargeThumbnailURL = Uri.parse(largeThumbnail);
            }
            video.mExpirationDate = expirationDate;
            video.mPerferredStream = preferredStream;
            video.mStreams = streams;

            video.mIsStream = isStream;
            video.mVideoParts = info;
            video.mPlayerScript = playerScript;

            return video;
        }
        else {
            String reason = (String) info.get("reason");
            if (null != reason) {
                reason = reason.replaceAll("<br\\s*/?>", " ");
                reason = reason.replaceAll("\n", "");

                Pattern pattern = Pattern.compile("<[^>]+>");
                Matcher matcher = pattern.matcher(reason);
                reason = matcher.replaceAll("");
            }
            else {
                reason = YoutubeErrorString.NO_STREAM_AVAILABLE_ERROR;
            }

            String errorcode = (String) info.get("errorcode");
            if (null != errorcode) {
                int code = Integer.parseInt(errorcode);
                throw new YoutubeErrorException(code, reason);
            }
            else {
                throw new NoStreamAvailableException(reason);
            }

        }

    }

    private static Date getExpirationDate(Uri streamURL) {
        HashMap<String, String> query = hashMapForQueryString(streamURL.getQuery());
        double expire = Double.parseDouble(query.get("expire"));
        long timeInterval = (long) (expire*1000);
        return (timeInterval > 0? new Date(timeInterval) : null);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private static Uri URLBySettingParameter(Uri URL, String key, String percentEncodedValue) {
        String pattern = "((?:^|&)" + key + "=)[^&]*";
        String template = "$1" + percentEncodedValue;

        Pattern regularExpression = Pattern.compile(pattern);
        String percentEncodedQuery = URL.getQuery();
        if (null == percentEncodedQuery) {
            percentEncodedQuery = "";
        }

        Matcher matcher = regularExpression.matcher(percentEncodedQuery);
        matcher.replaceAll(template);
        matcher.reset();
        int numberOfMatches = 0;
        if (matcher.find()) {
            numberOfMatches ++;
        }
        if (numberOfMatches == 0) {
            percentEncodedQuery += ((percentEncodedQuery.length() > 0?"&":"") + key + "=" + percentEncodedValue);
        }

        return URL.buildUpon().encodedQuery(percentEncodedQuery).build();
    }

    // 因為有些影片取得的url不能直接播放(我想是因為有版權問題)，所以需要針對有"s"和"sig"欄位的影片做signature解密，然後串在url後面
    @SuppressWarnings("deprecation")
    @Deprecated
    private static HashMap<String, String> parseRealVideoURL(HashMap<String, String> videoComponents) {
        if (null == videoComponents) {
            return null;
        }

        String s = videoComponents.get("s");
        if (null == s) {
            s = videoComponents.get("sig");
        }
        if (null == s) {
            return videoComponents;
        }
        if (s.length() == 81) {
            // e.g. BB3D19F5B62D78A1050E4EDCB43CC7603992C757.4BF9D97A31CA6238FBB6595735FEDE8BC1BF4001
            // 下面else的解析法用在這邊我想也可以成功 (沒試過)

            char[] chars = s.toCharArray();
            int[][] swapIndexes = {{68, 80}, {19, 80}, {11, 19}, {10, 50}, {0, 50}};
            for (int[] indexes : swapIndexes) {
                if (indexes.length != 2) {
                    continue;
                }

                char c = chars[indexes[0]];
                chars[indexes[0]] = chars[indexes[1]];
                chars[indexes[1]] = c;
            }
            String signature = String.valueOf(chars);

            String urlString = videoComponents.get("url");
            urlString = urlString + "&signature="+signature;
            videoComponents.put("url", urlString);
        }
        else {
            // e.g. 6262DD539A193C92BBC38C2AA450100D5EE31A236E.B206DA36E17C0114BB808171F854CFBDE23B6DBAA
            // Reference: https://github.com/jeckman/YouTube-Downloader/issues/9
            // 是否對所有影片都有用就不確定了

            ArrayList<String> strings = new ArrayList<>();
            Collections.addAll(strings, s.split(""));
            Collections.reverse(strings);

            List<String> a = strings.subList(1, strings.size()-1);
            Collections.reverse(a);

            a = a.subList(3, a.size()-1);
            strings.clear();
            strings.addAll(a);

            strings = XD(strings, 19);
            Collections.reverse(strings);

            strings = XD(strings, 35);
            strings = XD(strings, 61);
            a = strings.subList(2, strings.size()-1);

            String signature = "";
            for (String aa : a) {
                signature += aa;
            }

            String urlString = videoComponents.get("url");
            urlString = urlString + "&signature="+signature;
            videoComponents.put("url", urlString);
        }

        return videoComponents;
    }

    // Reference: https://github.com/jeckman/YouTube-Downloader/issues/9
    // 因為是參考html5-player.js裡的函式寫法重建的，所以函式名稱和引數名稱都是亂取的，
    // 不過具體作用就是交換陣列裡的兩個元素
    @Deprecated
    private static ArrayList<String> XD(ArrayList<String> a, int b) {
        String c = a.get(0);
        a.set(0, a.get( (b%a.size()) ));
        a.set(b, c);
        return a;
    }

    // 解析videoID
    public static String youtubeIDFromYoutubeURL(String youtubeURLString) {
        Uri youtubeURL = Uri.parse(youtubeURLString);
        if (null == youtubeURL) {
            return null;
        }

        String youtubeHost = youtubeURL.getHost();
        String[] youtubePathComponents = youtubeURL.getPath().split("/");
        if (null != youtubeHost && youtubePathComponents.length > 0) {
            String youtubeAbsoluteString = youtubeURL.toString();
            if (youtubeHost.equals("youtu.be")) {
                return youtubePathComponents[1];
            }
            else if (youtubeAbsoluteString.contains("www.youtube.com/embed")) {
                return youtubePathComponents[2];
            }
            else if (youtubeAbsoluteString.contains("www.youtube.com/c")) {
                // e.g. https://www.youtube.com/c/newsebc/live 原網址不包含videoID
                // (特例中的特例)
                return null;
            }
            else if (youtubeHost.equals("youtube.googleapis.com") || youtubePathComponents[0].equals("www.youtube.com")) {
                return youtubePathComponents[2];
            }
            else {
                HashMap<String, String> queryString = hashMapForQueryString(youtubeURL.getQuery());
                String searchParam = queryString.get("v");
                if (null != searchParam) {
                    return searchParam;
                }
            }
        }

        return null;
    }

    public static HashMap<String, String> hashMapForQueryString(String queryString) {
        HashMap<String, String> map = new HashMap<>();
        if (null == queryString) {
            return map;
        }

        return hashMapFromQueryStringComponents(queryString);
    }


    public static String stringByEncodingURLFormat(String string) {
        if (null == string) {
            return null;
        }

        String string1 = string;
        try {
            string1 = URLEncoder.encode(string1, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        string1 = string1.replace(" ", "+");

        return string1;
    }

    public static String stringByDecodingURLFormat(String string) {
        if (null == string) {
            return null;
        }

        String string1 = string.replace("+", " ");
        try {
            string1 = URLDecoder.decode(string1, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return string1;
    }

    public static HashMap<String, String> hashMapFromQueryStringComponents(String string) {
        HashMap<String, String> map = new HashMap<>();
        if (null == string) {
            return map;
        }

        for (String keyValue : string.split("&")) {
            String[] keyValueArray = keyValue.split("=");
            if (keyValueArray.length < 1) {
                continue;
            }

            String key = stringByDecodingURLFormat(keyValueArray[0]);
            if (keyValueArray.length < 2) {
                map.put(key, "");
                continue;
            }

            String value = stringByDecodingURLFormat(keyValueArray[1]);
            map.put(key, value);
        }
        return map;
    }

    @Override
    public String toString() {
        return "[" + mIdentifier + "]" + mTitle;
    }

}
