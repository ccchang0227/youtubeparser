package realtouch.youtubeparserkit.object;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import realtouch.youtubeparserkit.core.YoutubeParser;
import realtouch.youtubeparserkit.error.YoutubeErrorString;

/**
 * Created by C.C.Chang on 2017/2/21.
 *
 * @version 1
 */
@SuppressWarnings("unused")
public class YoutubeVideoQueue extends ArrayList<String> {
    private HashMap<String, YoutubeVideo> mVideos = new HashMap<>();

    public YoutubeVideoQueue(int initialCapacity) {
        super(initialCapacity);
    }

    public YoutubeVideoQueue() {
        super();
    }

    public YoutubeVideoQueue(@NonNull Collection<? extends String> c) {
        super(c);
    }

    public interface QueueCallback {
        void onSuccessful(int index, String source, YoutubeVideo video);
        void onFailed(int index, String source, String failedReason);
    }

    public void loadVideo(final int index, final QueueCallback callback) {
        if (index < 0 || index >= size()) {
            if (null != callback) {
                callback.onFailed(index, null, YoutubeErrorString.VIDEO_NOT_FOUND_ERROR);
            }
            return;
        }
        String sourceString = get(index);
        if (null == sourceString) {
            if (null != callback) {
                callback.onFailed(index, null, YoutubeErrorString.VIDEO_NOT_FOUND_ERROR);
            }
            return;
        }

        YoutubeVideo existVideo = mVideos.get(sourceString);
        if (null != existVideo) {
            if (null != callback) {
                callback.onSuccessful(index, sourceString, existVideo);
            }
            return;
        }

        YoutubeParser.getInstance().getVideoInfo(sourceString, new YoutubeParser.ResultCallback() {
            @Override
            public void onSuccessful(String source, YoutubeVideo video) {
                if (null != video) {
                    YoutubeVideoQueue.this.mVideos.put(source, video);
                    if (null != callback) {
                        callback.onSuccessful(index, source, video);
                    }
                }
                else {
                    if (null != callback) {
                        callback.onFailed(index, source, YoutubeErrorString.VIDEO_NOT_FOUND_ERROR);
                    }
                }
            }

            @Override
            public void onFailed(String source, String failedReason) {
                if (null != callback) {
                    callback.onFailed(index, source, failedReason);
                }
            }
        });
    }

    // MARK - Override

    @Override
    public void clear() {
        mVideos.clear();

        super.clear();
    }

}
