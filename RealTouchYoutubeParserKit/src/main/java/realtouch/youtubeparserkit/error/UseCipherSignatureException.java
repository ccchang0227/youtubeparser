package realtouch.youtubeparserkit.error;

/**
 * Created by C.C.Chang on 2017/5/26.
 *
 * @version 1
 */
@SuppressWarnings("unused")
public final class UseCipherSignatureException extends Exception {

    public UseCipherSignatureException() {
        super();
    }

    public UseCipherSignatureException(String message) {
        super(message);
    }

}
