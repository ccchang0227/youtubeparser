package realtouch.youtubeparserkit.error;

/**
 * Created by C.C.Chang on 2017/5/26.
 *
 * @version 1
 */
@SuppressWarnings("unused")
public final class YoutubeErrorString {
    public final static String NO_VIDEO_URL_ERROR = "無影片網址";
    public final static String VIDEO_NOT_FOUND_ERROR = "找不到影片";
    public final static String PARSE_FAILED_ERROR = "解析失敗";

    public final static String NO_STREAM_AVAILABLE_ERROR = "找不到影片";
}
