package realtouch.youtubeparserkit.error;

/**
 * Created by C.C.Chang on 2017/5/26.
 *
 * @version 1
 */
@SuppressWarnings("unused")
public final class NoStreamAvailableException extends Exception {

    public NoStreamAvailableException() {
        super();
    }

    public NoStreamAvailableException(String message) {
        super(message);
    }

}
