package realtouch.youtubeparserkit.error;

/**
 * Created by C.C.Chang on 2017/5/31.
 *
 * @version 1
 */
@SuppressWarnings("unused")
public final class YoutubeErrorException extends Exception {

    private int mCode = 0;

    public YoutubeErrorException() {
        super();
    }

    public YoutubeErrorException(String message) {
        super(message);
    }

    public YoutubeErrorException(int errorCode, String message) {
        super(message);

        this.mCode = errorCode;
    }

    public int getErrorCode() {
        return mCode;
    }

}
