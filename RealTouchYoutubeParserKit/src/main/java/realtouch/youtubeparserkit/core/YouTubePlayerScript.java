package realtouch.youtubeparserkit.core;

import org.liquidplayer.webkit.javascriptcore.JSContext;
import org.liquidplayer.webkit.javascriptcore.JSException;
import org.liquidplayer.webkit.javascriptcore.JSObject;
import org.liquidplayer.webkit.javascriptcore.JSValue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by C.C.Chang on 2017/5/26.
 *
 * @version 1
 */
@SuppressWarnings("unused")
public final class YouTubePlayerScript {

    private JSContext mContext = new JSContext();
    private JSValue mSignatureFunction;

    YouTubePlayerScript(String string) {
        super();

        mContext.setExceptionHandler(new JSContext.IJSExceptionHandler() {
            @Override
            public void handle(JSException exception) {
                exception.printStackTrace();
            }
        });

        HashMap<String, JSValue> environment = new HashMap<>();

        HashMap<String, JSValue> documentElement = new HashMap<>();
        documentElement.put("documentElement", new JSObject(mContext));
        environment.put("document", new JSObject(mContext, documentElement));
        HashMap<String, JSValue> location = new HashMap<>();
        location.put("hash", new JSValue(mContext, ""));
        environment.put("location", new JSObject(mContext, location));
        HashMap<String, JSValue> navigator = new HashMap<>();
        navigator.put("userAgent", new JSValue(mContext, ""));
        environment.put("navigator", new JSObject(mContext, navigator));

        JSObject window = new JSObject(mContext);
        for (String propertyName : environment.keySet()) {
            JSValue value = environment.get(propertyName);
            mContext.property(propertyName, value);
            window.property(propertyName, value);
        }
        mContext.property("window", window);

        String script = string;//.replaceAll("\\u0020", "");
//        script = script.replaceAll("\\u0009", "");
//        script = script.replaceAll("\\u000A", "");
//        script = script.replaceAll("\\u000B", "");
//        script = script.replaceAll("\\u000C", "");
//        script = script.replaceAll("\\u000D", "");
//        script = script.replaceAll("\\u0085", "");
        mContext.evaluateScript(script);

        Pattern anonymousFunctionRegularExpression = Pattern.compile("\\(function\\(([^)]*)\\)\\{(.*)\\}\\)\\(([^)]*)\\)", Pattern.DOTALL);
        Matcher matcher = anonymousFunctionRegularExpression.matcher(script);
        ArrayList<MatchResult> anonymousFunctionResults = new ArrayList<>();
        if (matcher.find()) {
            anonymousFunctionResults.add(matcher.toMatchResult());
        }
        if (anonymousFunctionResults.size() > 0) {
            MatchResult anonymousFunctionResult = anonymousFunctionResults.get(0);
            if (anonymousFunctionResult.groupCount() > 2) {
                String[] parameters = anonymousFunctionResult.group(1).split(",");
                String[] arguments = anonymousFunctionResult.group(3).split(",");
                if (parameters.length == arguments.length) {
                    for (int i = 0; i < parameters.length; i ++) {
                        mContext.property(parameters[i], mContext.property(arguments[i]));
                    }
                }
                String anonymousFunctionBody = anonymousFunctionResult.group(2);
                mContext.evaluateScript(anonymousFunctionBody);
            }
        }

        Pattern signatureRegularExpression = Pattern.compile("[\"']signature[\"']\\s*,\\s*([^\\(]+)", Pattern.CASE_INSENSITIVE);
        matcher = signatureRegularExpression.matcher(script);
        ArrayList<MatchResult> signatureResults = new ArrayList<>();
        while (matcher.find()) {
            signatureResults.add(matcher.toMatchResult());
        }
        for (MatchResult signatureResult : signatureResults) {
            String signatureFunctionName = null;
            if (signatureResult.groupCount() > 0) {
                signatureFunctionName = signatureResult.group(1);
            }
            if (null == signatureFunctionName) {
                continue;
            }

            JSValue signatureFunction = mContext.property(signatureFunctionName);
            if (signatureFunction.isObject()) {
                mSignatureFunction = signatureFunction;
                break;
            }
        }

    }

    public String unscrambleSignature(String scrambledSignature) {
        if (null == mSignatureFunction || null == scrambledSignature) {
            return null;
        }

        JSValue unscrambledSignature = mSignatureFunction.toFunction().call(null, scrambledSignature);
        return (unscrambledSignature.isString()? unscrambledSignature.toString() : null);
    }

}
