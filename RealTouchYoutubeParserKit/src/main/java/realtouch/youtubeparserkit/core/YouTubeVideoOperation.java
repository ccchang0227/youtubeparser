package realtouch.youtubeparserkit.core;

import android.os.Handler;
import android.os.Looper;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import realtouch.youtubeparserkit.error.UseCipherSignatureException;
import realtouch.youtubeparserkit.error.YoutubeErrorString;
import realtouch.youtubeparserkit.object.YoutubeVideo;

/**
 * Created by C.C.Chang on 2017/5/31.
 *
 * @version 1
 */
@SuppressWarnings("unused")
public final class YouTubeVideoOperation {

    public interface ResultCallback {
        // 影片解析成功
        void onSuccessful(YouTubeVideoOperation operation, YoutubeVideo video);
        // 影片解析失敗
        void onFailed(YouTubeVideoOperation operation, String failedReason);
    }

    private Handler mHandler; // 用於強制到ui線程執行

    private final static String mYoutubeVideoInfoAPIBaseUrl = "https://www.youtube.com/get_video_info?";
    private final static String mYoutubeVideoWatchPageBaseUrl = "https://www.youtube.com/watch?";
    private final static String mYoutubeVideoEmbededPageBaseUrl = "https://www.youtube.com/embed/";
    private final static String mUserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.79 Safari/537.4";

    private int mEventLabelIndex = 0;
    private final static String[] mEventLabels = {"embedded", "detailpage"};

    private enum RequestType {
        GetVideoInfo(1),
        WatchPage(2),
        EmbedPage(3),
        JavaScriptPlayer(4);

        private int value;
        RequestType(int i) {
            this.value = i;
        }

    }
    private RequestType mRequestType;

    private String mSource = null;
    private String mLanguageIdentifier = null;
    private ResultCallback mCallback = null;

    private String mVideoID = null;
    private YouTubePlayerScript mPlayerScript = null;
    private YouTubeVideoWebpage mWebpage = null;
    private YouTubeVideoWebpage mEmbedWebpage = null;

    private int mRequestCount = 0;
    private Call mCurrentCall = null;
    private String mLastError = null;

    private boolean mExecuting = false;
    private boolean mFinished = false;
    private boolean mCancelled = false;

    YouTubeVideoOperation(String source, String languageIdentifier, ResultCallback callback) {
        super();

        this.mHandler = new Handler(Looper.getMainLooper());

        this.mSource = source;
        this.mCallback = callback;

        if (null == languageIdentifier) {
            this.mLanguageIdentifier = "en";
        }
        else {
            this.mLanguageIdentifier = languageIdentifier;
        }

        this.mVideoID = YoutubeVideo.youtubeIDFromYoutubeURL(this.mSource);
        if (null == mVideoID && mSource.length() == 11) {
            this.mVideoID = mSource;
        }

    }

    public String getSource() {
        return mSource;
    }

    public String getVideoID() {
        return mVideoID;
    }

    public YouTubeVideoOperation start() {
        if (mCancelled) {
            return this;
        }
        if (mExecuting) {
            return this;
        }

        mExecuting = true;

        mRequestCount = 0;
        mEventLabelIndex = 0;
        startNextRequest();

        return this;
    }

    public void cancel() {
        if (mCancelled || mFinished) {
            return;
        }

        mCancelled = true;

        if (null != mCurrentCall) {
            if (mCurrentCall.isExecuted() && !mCurrentCall.isCanceled()) {
                mCurrentCall.cancel();
            }
        }

        finish();
    }

    // MARK - Private

    private void startNextRequest() {
        if (mEventLabelIndex >= mEventLabels.length) {
            if (mRequestType == RequestType.WatchPage || null != mWebpage) {
                finishWithError();
            }
            else {
                startWatchPageRequest();
            }

            return;
        }
        if (null == mVideoID || mVideoID.length() != 11) {
            startWatchPageRequest();
            return;
        }

        String el = mEventLabels[mEventLabelIndex];
        mEventLabelIndex ++;

        String url = mYoutubeVideoInfoAPIBaseUrl + "video_id=" + ((null != mVideoID)?mVideoID:"")
                                                + "&hl=" + mLanguageIdentifier
                                                + "&el=" + el
                                                + "&ps=default";
        startRequest(url, RequestType.GetVideoInfo);
    }

    private void startWatchPageRequest () {
        String url;
        if (null == mVideoID || mVideoID.length() != 11) {
            url = mSource;
        }
        else {
            url = mYoutubeVideoWatchPageBaseUrl + "v=" + mVideoID
                                                + "&hl=" + mLanguageIdentifier
                                                + "&has_verified=1";
        }

        startRequest(url, RequestType.WatchPage);
    }

    private void startRequest(String url, final RequestType requestType) {
        if (mCancelled) {
            return;
        }

        if (++mRequestCount > 6) {
            finishWithError();
            return;
        }

        this.mRequestType = requestType;

        OkHttpClient client = new OkHttpClient().newBuilder()
                                                .connectTimeout(10, TimeUnit.SECONDS)
                                                .retryOnConnectionFailure(false)
                                                .build();
        Request request = new Request.Builder()
                                    .get()
                                    .url(url)
                                    .addHeader("User-Agent", mUserAgent) // 用User-Agent偽裝是從瀏覽器送的請求
                                    .addHeader("Accept-Language", mLanguageIdentifier)
                                    .build();
        mCurrentCall = client.newCall(request);
        mCurrentCall.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                if (YouTubeVideoOperation.this.mCancelled) {
                    return;
                }

                YouTubeVideoOperation.this.handleConnectionError(e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (YouTubeVideoOperation.this.mCancelled) {
                    return;
                }

                String responseString = response.body().string();
                YouTubeVideoOperation.this.handleConnectionSuccess(responseString, requestType);
            }
        });
    }

    private void handleConnectionSuccess(String responseString, RequestType requestType) {
        switch (requestType) {
            case GetVideoInfo: {
                handleVideoInfoResponse(YoutubeVideo.hashMapForQueryString(responseString));
                break;
            }
            case WatchPage: {
                handleWebPage(responseString);
                break;
            }
            case EmbedPage: {
                handleEmbedWebPage(responseString);
                break;
            }
            case JavaScriptPlayer: {
                handleJavaScriptPlayer(responseString);
                break;
            }
        }
    }

    private void handleConnectionError(Exception e) {
        e.printStackTrace();
        mLastError = e.getLocalizedMessage();

        startNextRequest();
    }

    private void handleVideoInfoResponse(Map info) {
        try {
            YoutubeVideo video = YoutubeVideo.youtubeVideoWith(mVideoID, mPlayerScript, info);
            finish(video);

        } catch (UseCipherSignatureException e) {
            e.printStackTrace();

            startWatchPageRequest();

        } catch (Exception e) {
            e.printStackTrace();

            mLastError = e.getLocalizedMessage();
            startNextRequest();
        }
    }

    private void handleWebPage(String html) {
        mWebpage = new YouTubeVideoWebpage(html);
        if (null == mVideoID || mVideoID.length() != 11) {
            mVideoID = mWebpage.getVideoID();
        }

        if (null != mWebpage.getJavaScriptPlayerURL()) {
            startRequest(mWebpage.getJavaScriptPlayerURL().toString(), RequestType.JavaScriptPlayer);
        }
        else {
            if (mWebpage.isAgeRestricted()) {
                String url = mYoutubeVideoEmbededPageBaseUrl + mVideoID;
                startRequest(url, RequestType.EmbedPage);
            }
            else {
                startNextRequest();
            }
        }

    }

    private void handleEmbedWebPage(String html) {
        mEmbedWebpage = new YouTubeVideoWebpage(html);
        if (null != mEmbedWebpage.getJavaScriptPlayerURL()) {
            startRequest(mEmbedWebpage.getJavaScriptPlayerURL().toString(), RequestType.JavaScriptPlayer);
        }
        else {
            startNextRequest();
        }
    }

    private void handleJavaScriptPlayer(String script) {
        mPlayerScript = new YouTubePlayerScript(script);
        if (mWebpage.isAgeRestricted()) {
            String eurl = "https://youtube.googleapis.com/v/" + mVideoID;
            String sts;
            if (null != mEmbedWebpage) {
                String embededSts = mEmbedWebpage.getPlayerConfiguration().get("sts").toString();
                if (null != embededSts) {
                    sts = embededSts;
                }
                else {
                    sts = mWebpage.getPlayerConfiguration().get("sts").toString();
                }
                if (null == sts) {
                    sts = "";
                }
                String url = mYoutubeVideoInfoAPIBaseUrl + "video_id=" + mVideoID
                                                        + "&hl=" + mLanguageIdentifier
                                                        + "&eurl=" + eurl
                                                        + "&sts=" + sts;
                startRequest(url, RequestType.GetVideoInfo);
            }
        }
        else {
            handleVideoInfoResponse(mWebpage.getVideoInfo());
        }
    }

    private void finish(final YoutubeVideo video) {
        finish();

        if (null != mCallback) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    YouTubeVideoOperation.this.mCallback.onSuccessful(YouTubeVideoOperation.this, video);
                }
            });
        }
    }

    private void finishWithError() {
        finish();

        if (null != mCallback) {
            if (null == mLastError) {
                mLastError = YoutubeErrorString.VIDEO_NOT_FOUND_ERROR;
            }

            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    YouTubeVideoOperation.this.mCallback.onFailed(YouTubeVideoOperation.this, YouTubeVideoOperation.this.mLastError);
                }
            });
        }
    }

    private void finish() {
        mExecuting = false;
        mFinished = true;
    }

}
