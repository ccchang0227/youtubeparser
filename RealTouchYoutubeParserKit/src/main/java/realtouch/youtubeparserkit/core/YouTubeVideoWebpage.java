package realtouch.youtubeparserkit.core;

import android.net.Uri;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static realtouch.youtubeparserkit.tools.JsonUtils.toMap;

/**
 * Created by C.C.Chang on 2017/5/26.
 *
 * @version 1
 */
@SuppressWarnings("unused")
public final class YouTubeVideoWebpage {

    private String mHtml = null;

    private Map<String, Object> mPlayerConfiguration = null;
    private Map<Object, Object> mVideoInfo = null;
    private Uri mJavaScriptPlayerURL = null;
    private boolean mAgeRestricted = false;
    private Set<String> mRegionsAllowed = null;

    private String mVideoID = null;

    YouTubeVideoWebpage(String html) {
        super();

        this.mHtml = html;

    }

    public Map<String, Object> getPlayerConfiguration() {
        if (null == mPlayerConfiguration) {
            Pattern playerConfigRegularExpression = Pattern.compile("ytplayer.config\\s*=\\s*(\\{.*?\\});|[\\({]\\s*'PLAYER_CONFIG'[,:]\\s*(\\{.*?\\})\\s*(?:,'|\\))", Pattern.CASE_INSENSITIVE);
            Matcher matcher = playerConfigRegularExpression.matcher(this.mHtml);
            boolean stop = false;
            while (matcher.find()) {
                for (int i = 1; i < matcher.groupCount()+1; i ++) {
                    String configString = matcher.group(i);
                    if (null != configString) {
                        try {
                            JSONObject playerConfiguration = new JSONObject(configString);
                            this.mPlayerConfiguration = toMap(playerConfiguration);
                            stop = true;

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                if (stop) {
                    break;
                }
            }
        }

        return mPlayerConfiguration;
    }

    public Map<Object, Object> getVideoInfo() {
        if (null == mVideoInfo) {
            try {
                Map args = (Map) this.getPlayerConfiguration().get("args");
                HashMap<Object, Object> info = new HashMap<>();
                for (Object key : args.keySet()) {
                    Object value = args.get(key);
                    if (value instanceof CharSequence || value instanceof Number) {
                        info.put(key, value.toString());
                    }
                }
                this.mVideoInfo = info;

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return mVideoInfo;
    }

    public Uri getJavaScriptPlayerURL() {
        if (null == mJavaScriptPlayerURL) {
            try {
                String jsAssets = (String) ((Map) this.getPlayerConfiguration().get("assets")).get("js");
                if (null != jsAssets) {
                    String javaScriptPlayerURLString = jsAssets;
                    if (jsAssets.startsWith("//")) {
                        javaScriptPlayerURLString = "https" + jsAssets;
                    }
                    else if (jsAssets.startsWith("/")) {
                        javaScriptPlayerURLString = "https://www.youtube.com" +jsAssets;
                    }
                    this.mJavaScriptPlayerURL = Uri.parse(javaScriptPlayerURLString);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return mJavaScriptPlayerURL;
    }

    public boolean isAgeRestricted() {
        if (!mAgeRestricted) {
            this.mAgeRestricted = this.mHtml.contains("og:restrictions:age");
        }

        return mAgeRestricted;
    }

    public Set<String> getRegionsAllowed() {
        if (null == mRegionsAllowed) {
            HashSet<String> regionsAllowedSet = new HashSet<>();
            Pattern regionsAllowedRegularExpression = Pattern.compile("meta\\s+itemprop=\"regionsAllowed\"\\s+content=\"(.*)\"");
            Matcher regionsAllowedResult = regionsAllowedRegularExpression.matcher(this.mHtml);
            if (regionsAllowedResult.find()) {
                if (regionsAllowedResult.groupCount() > 0) {
                    String regionsAllowed = regionsAllowedResult.group(1);
                    if (regionsAllowed.length() > 0) {
                        regionsAllowedSet.addAll(Arrays.asList(regionsAllowed.split(",")));
                    }
                }
            }
            mRegionsAllowed = regionsAllowedSet;

        }

        return mRegionsAllowed;
    }

    public String getVideoID() {
        if (null == mVideoID) {
            Document document = Jsoup.parse(this.mHtml);
            if (null != document) {
                Elements elements = document.getElementsByTag("meta");
                if (null != elements) {
                    for (Element element : elements) {
                        if (element.hasAttr("itemprop")) {
                            if (element.attr("itemprop").equals("videoId")) {
                                mVideoID = element.attr("content");
                                break;
                            }
                        }
                    }
                }
            }

        }

        return mVideoID;
    }

}
