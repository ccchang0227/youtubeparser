package realtouch.youtubeparserkit.core;

import realtouch.youtubeparserkit.object.YoutubeVideo;

/**
 * Created by Chih-chieh Chang on 2017-2-20.
 *
 * @version 1.0.0
 */
@SuppressWarnings("unused")
public final class YoutubeParser {
    private static YoutubeParser ourInstance = new YoutubeParser();

    public static YoutubeParser getInstance() {
        return ourInstance;
    }

    private String mLanguageIdentifier = "en";

    private YoutubeParser() {
        super();
    }

    public interface ResultCallback {
        // 影片解析成功
        void onSuccessful(String source, YoutubeVideo video);
        // 影片解析失敗
        void onFailed(String source, String failedReason);
    }

    public void setLanguageIdentifier(String languageIdentifier) {
        this.mLanguageIdentifier = languageIdentifier;
    }

    public YouTubeVideoOperation getVideoInfo(final String source, final ResultCallback callback) {
        YouTubeVideoOperation operation = new YouTubeVideoOperation(source, mLanguageIdentifier, new YouTubeVideoOperation.ResultCallback() {
            @Override
            public void onSuccessful(YouTubeVideoOperation operation, YoutubeVideo video) {
                if (null != callback) {
                    callback.onSuccessful(source, video);
                }
            }

            @Override
            public void onFailed(YouTubeVideoOperation operation, String failedReason) {
                if (null != callback) {
                    callback.onFailed(source, failedReason);
                }
            }
        });

        return operation.start();
    }

}
