package realtouch.youtubeparserkit.tools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by C.C.Chang on 2017/5/26.
 *
 * @version 1
 */
@SuppressWarnings("unused")
public final class JsonUtils {

    public static Map<String, Object> jsonToMap(JSONObject json) {
        Map<String, Object> retMap = new HashMap<>();

        if (json != JSONObject.NULL) {
            retMap = toMap(json);
        }
        return retMap;
    }

    public static Map<String, Object> toMap(JSONObject object) {
        if (object == JSONObject.NULL) {
            return null;
        }

        Map<String, Object> map = new HashMap<>();
        Iterator<String> keysItr = object.keys();
        while(keysItr.hasNext()) {
            String key = keysItr.next();
            try {
                Object value = object.get(key);
                if(value instanceof JSONArray) {
                    value = toList((JSONArray) value);
                }

                else if(value instanceof JSONObject) {
                    value = toMap((JSONObject) value);
                }
                map.put(key, value);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return map;
    }

    public static List<Object> toList(JSONArray array) {
        if (array == JSONObject.NULL) {
            return null;
        }

        List<Object> list = new ArrayList<>();
        for(int i = 0; i < array.length(); i ++) {
            try {
                Object value = array.get(i);
                if(value instanceof JSONArray) {
                    value = toList((JSONArray) value);
                }

                else if(value instanceof JSONObject) {
                    value = toMap((JSONObject) value);
                }
                list.add(value);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return list;
    }
}
