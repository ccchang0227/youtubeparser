# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [0.2.0] - 2017-06-01
### Changed
- 參考iOS的[XCDYouTubeKit](https://github.com/0xced/XCDYouTubeKit)重寫library
- library版本更新到0.2.0(2)
- app版本更新到1.1.0(2)

## [0.0.5] - 2017-03-04
### Added
#### - RealTouchYoutubeParserKit [Author: @ccchang0227]
- YoutubeVideo: 基本的解析函式 和 存放解析完成的影片資訊
- YoutubeParser: 主要和youtube API獲取影片資訊的class (Singleton)
- YoutubeVideoQueue: 用於存放影片列表
